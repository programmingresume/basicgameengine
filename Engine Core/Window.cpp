#include "window.h"

namespace jEngine 
{
	namespace graphics 
	{
		void windowresize(GLFWwindow *window, int width, int height);

		Window::Window(const char *title, int width, int height) 
		{
			m_Title = title;
			m_Width = width;
			m_Height = height;

			if (!init())
				glfwTerminate();

			for (int i = 0; i < MAX_KEYS; i++) 
			{
				m_Keys[i] = false;
			}
			for (int i = 0; i < MAX_BUTTONS; i++)
			{
				m_MouseButtons[i] = false;
			}
		}

		Window::~Window() 
		{
			glfwTerminate();
		}

		void Window::clear() const
		{
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		}

		void Window::update()
		{
			glfwPollEvents();
			glfwSwapBuffers(m_Window);
		}

		void windowresize(GLFWwindow *window, int width, int height) 
		{
			glViewport(0, 0, width, height);
		}

		bool Window::closed() const 
		{
			return glfwWindowShouldClose(m_Window) == 1;
		}

		bool Window::init() 
		{
			if (!glfwInit()) {
				std::cout << "Error: GLFW Failed to Initialise" << std::endl;
				return false;
			}

			m_Window = glfwCreateWindow(m_Width, m_Height, m_Title, NULL, NULL);
			if (!m_Window) 
			{
				glfwTerminate();
				std::cout << "Failed to Create Window" << std::endl;
				return false;
			}

			glfwMakeContextCurrent(m_Window);
			glfwSetWindowUserPointer(m_Window, this);
			glfwSetWindowSizeCallback(m_Window, windowresize);
			glfwSetKeyCallback(m_Window, key_callback);
			glfwSetMouseButtonCallback(m_Window, mouse_button_callback);
			glfwSetCursorPosCallback(m_Window, cursor_position_callback);

			if (glewInit() != GLEW_OK) 
			{
				std::cout << "GLEW Could not be Initialised" << std::endl;
				return false;
			}

			std::cout << "OpenGL" << glGetString(GL_VERSION) << std::endl;

			return true;
		}

		bool Window::isKeyPressed(unsigned int keycode) const
		{
			//TODO: Log
			if (keycode >= MAX_KEYS)
				return false;
			return m_Keys[keycode];
		}

		bool Window::isMouseButtonPressed(unsigned int button) const
		{
			//TODO: Log
			if (button >= MAX_KEYS)
				return false;
			return m_MouseButtons[button];
		}

		void Window::getMousePosition(double& x, double& y) const
		{
			x = mx;
			y = my;
		}

		static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods) 
		{
			Window* win = (Window*)glfwGetWindowUserPointer(window);
			
			win->m_Keys[key] = action != GLFW_RELEASE;
		}

		static void mouse_button_callback(GLFWwindow* window, int button, int action, int mods)
		{
			Window* win = (Window*)glfwGetWindowUserPointer(window);
			win->m_MouseButtons[button] = action != GLFW_RELEASE;
		}

		static void cursor_position_callback(GLFWwindow* window, double xpos, double ypos)
		{
			Window* win = (Window*)glfwGetWindowUserPointer(window);
			win->mx = xpos;
			win->my = ypos;
		}
	}
}