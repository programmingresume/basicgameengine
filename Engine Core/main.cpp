#include "window.h"
#include "maths.h"
#include "Shader.h"

int main() 
{
	using namespace jEngine;
	using namespace graphics;
	using namespace maths;

	Window window("jEngine!", 960, 540);
	// glClearColor(1.0f, 1.0f, 1.0f, 1.0f);

	

	GLfloat verticies[] =
	{
		 0, 0, 0,
		 8, 0, 0,
		 0, 3, 0,
		 0, 3, 0,
		 8, 3, 0,
		 8, 0, 0
	};

	GLuint vbo;

	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(verticies), verticies, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(0);

	mat4 ortho = mat4::orthographic(0.0f, 16.0f, 0.0f, 9.0f, -1.0f, 1.0f);

	Shader shader("basic.vert", "basic.frag");
	shader.enable();
	shader.setUniformMat4("pr_matrix", ortho);
	shader.setUniformMat4("ml_matrix", mat4::translation(vec3(4, 3, 0)));

	shader.setUniform2f("light_pos", vec2(8.0f, 4.0f));
	shader.setUniform4f("colour", vec4(0.0f, 1.0f, 1.0f, 1.0f));

	vec3 rot = { 0, 0, 45 };

	ortho.rotation(45.0f, rot);

	while (!window.closed()) 
	{
		window.clear();
		glDrawArrays(GL_TRIANGLES, 0, 6);
		window.update();
	}
	return 0;
}